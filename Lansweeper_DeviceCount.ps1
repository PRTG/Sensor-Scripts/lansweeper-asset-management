# ___ ___ _____ ___
#| _ \ _ \_   _/ __|
#|  _/   / | || (_ |
#|_| |_|_\ |_| \___|
#    NETWORK MONITOR
#
#-------------------
# Name   Lansweeper_DeviceCount.ps1          
# Description   PRTG EXE/Script Sensor to retrieve asset counts from a Lansweeper cloud instance     
#
#-------------------
# Requirements
#
# Lansweeper Cloud instance
#
#-------------------
# Version History
# 
# Version  Date        Notes
# 1.0      05/07/2022  Initial Release
#
# ------------------
# (c) 2022 Simon Bell | Paessler AG
# ------------------

#Parameters
#
#Use the PRTG sensor 'parameter' field to specify -siteID, -token and -assetType search string


Param (
        [string]$siteID = $null,
        [string]$token = $null, 
        [string]$assetType = $null, 
    [int]$debuglevel = 0
)

#Log errors for missing parameters
if (-not $siteID) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide siteID </text>
</prtg>
"@
}

if (-not $token) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide API token</text>
</prtg>
"@
}

if (-not $assetType) {
    return @"
<prtg>
<error>1</error>
<text>Required parameter not specified: please provide Asset Type </text>
</prtg>
"@
}

#Setup API connection

$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("Content-Type", "application/json")
$headers.Add("Authorization", "Token $token")

$body = "{
`n    `"query`": `"{ site(id: `\`"$siteID`\`"){ assetResources( fields: [ `\`"assetBasicInfo.name`\`", `\`"assetBasicInfo.type`\`", `\`"url`\`" ], filters: { conjunction: AND, conditions: [ { operator: LIKE, path: `\`"assetBasicInfo.type`\`", value: `\`"$assetType`\`" } ] } ){total} }}`"
`n}"

#Make API call

$response = Invoke-RestMethod 'https://api.lansweeper.com/api/v2/graphql' -Method 'POST' -Headers $headers -Body $body
$response | ConvertTo-Json
$assetcount = $response.data.site.assetResources

#Extract object count from the API return

$value = $assetcount -split "@{total=(.*?)}.*"

#Send value to PRTG channel

Write-Host 
"<prtg>"
    "<result>"
        "<channel>" + $assetType +' Count' +"</channel>"
        "<value>"+ $value +"</value>"
    "</result>"
"</prtg>"